pipeline {
  environment {
    TAG_VERSION = 1.1
    IMAGE = 'compiler-service'
    DOCKER_HUB_USER = 'adalidflores'
    DOCKER_HUB_PASSWORD = credentials('docker_hub_pass')
    SONAR_TOKEN = credentials('sonar_token')
  }
  agent any
  stages {
    stage('build') {
      steps {
        sh 'docker build -t ${IMAGE}:${TAG_VERSION} .'
      }
    }
    stage('test') {
      steps {
        sh 'docker run --name tempfortest -d ${IMAGE}:${TAG_VERSION}'
        sh 'docker exec tempfortest npm run test'
      }
      post {
        always {
          sh 'docker cp tempfortest:/app/test-results.xml .'
          sh 'docker rm -f tempfortest'
          junit 'test-results.xml'
          archiveArtifacts artifacts: 'test-results.xml'
        }
      }
    }
    stage('publish') {
      steps {
        sh 'docker login -u "${DOCKER_HUB_USER}" -p "${DOCKER_HUB_PASSWORD}"'
        sh 'docker tag ${IMAGE}:${TAG_VERSION} ${DOCKER_HUB_USER}/${IMAGE}:${TAG_VERSION}'
        sh 'docker push ${DOCKER_HUB_USER}/${IMAGE}:${TAG_VERSION}'
      }
      post {
        always {
          sh 'docker image prune -f -a'
          archiveArtifacts artifacts: 'docker-compose.yml'
        }
      }
    }
    
    stage('sonar') {
      steps {
        sh 'sonar-scanner -Dsonar.organization=adalidflores -Dsonar.projectKey=adalidflores_if_personal_csproject -Dsonar.sources=. -Dsonar.host.url=https://sonarcloud.io'
      }
    }

    stage('deploy') {
      steps {
        copyArtifacts filter: 'docker-compose.yml', fingerprintArtifacts: true, projectName: '${JOB_NAME}', selector: specific('${BUILD_NUMBER}')
        sh 'docker-compose up -d'
      }
    }

    stage('acceptance') {
      steps {
        sh 'ls -la'
        sh 'echo "Aceptance stage"'
      }
    }
  }

  post {
    always {
        mail to: 'ivan.flores@jala-foundation.org',
        subject: "Pipeline completed: ${currentBuild.fullDisplayName} - ${currentBuild.currentResult}",
        body: "Process completed.\nSTARTED: Job ${env.JOB_NAME} [${env.BUILD_NUMBER}] \nSee full details in the next link: ${env.BUILD_URL}"
    }
  }
}