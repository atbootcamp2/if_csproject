# compilers_service
Jalasoft Automation BootCamp 01 - Project 01
Conceptual design can be found at [this link](https://drive.google.com/file/d/1WTLRFkkO_hoHeE8KobswkalNDI9yBK5y/view?usp=sharing)
On **Use Case Diagram** page.

## Requirements

1. Node.js 14.16.0

## Steps you should follow:
1. Install Dependencies:
```sh
npm install
```
2. Run project:
```sh
npm start
```
## Creating the database:
1. go the path "C:\Program Files\MongoDB\Server\4.4\bin"
2. execute mongod.exe
3. execute mongo.exe (the commands above must be run on this terminal)
4. run the command
```sh
use compilers_service_db
```
5. run the command
```sh
db.projects.save({name: 'first', binary_path: 'C:/users/python', project_path: 'C:/users/projects', language: 'python'})
```
6. your database is installed