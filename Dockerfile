FROM node AS build
COPY . /app
WORKDIR /app/src
RUN mv .env.example .env && \
    npm install

FROM node
WORKDIR /app
COPY --from=build /app/src .
RUN mkdir -p ../third_party/projects
EXPOSE 8000
ENTRYPOINT ["npm","start"]