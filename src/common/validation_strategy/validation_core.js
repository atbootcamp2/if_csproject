/*
 @validation_facade.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


const ContextValidation = require("./context_validation");
const EmptyOrNullValidation = require("./empty_or_null_validation");
const FolderOrFileExistsValidation = require("./path_exists_validation");
const LanguageExistsValidation = require("./language_exists_validation");


// builds ValidationCore class
class ValidationCore {

    // validates facade parameters
    facade_parameters(path_binary, path_projects, name_project, language){
        const facade_strategies = [
            new EmptyOrNullValidation(path_binary, "binary path"),
            new EmptyOrNullValidation(path_projects, "project path"),
            new EmptyOrNullValidation(name_project, "project name"),
            new FolderOrFileExistsValidation(path_binary, "binary file"),
            new FolderOrFileExistsValidation(path_projects, "project folder"),
            new LanguageExistsValidation(language)
        ];
        const context = new ContextValidation(facade_strategies);
        context.validate();
    }

    // validates command parameters
    command_parameters(parameters){
        const command_strategies = [
            new EmptyOrNullValidation(parameters, "command parameters"),
        ];
        const context = new ContextValidation(command_strategies);
        context.validate();
    }

    // validates parameters
    parameters(path_binary, path_projects, name_project){
        const command_strategies = [
            new EmptyOrNullValidation(path_binary, "binary path"),
            new EmptyOrNullValidation(path_projects, "project path"),
            new EmptyOrNullValidation(name_project, "project name"),
        ];
        const context = new ContextValidation(command_strategies);
        context.validate();
    }
}

// Exports ValidationCore class
module.exports = ValidationCore;
