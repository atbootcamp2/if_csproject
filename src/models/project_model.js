/*
 @project_model.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/

'use strict'

const mongoose = require('mongoose');
const schema = mongoose.Schema;
const project_schema = schema(
    {
        name: String,
        language: String
    },
    {
        versionKey: false
    }
);

// Exports module
module.exports = mongoose.model('projects', project_schema,'projects');
