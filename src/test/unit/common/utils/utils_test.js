/*
 @utils_test.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/


var expect = require("chai").expect;
const CompilersServiceErrorFileSystem = require('../../../../common/errors/compilers_service_error_file_system');
const CompilersServiceError = require('../../../../common/errors/compilers_service_error');
const Utils = require('./../../../../common/utils/utils')
const constants = require('./../../../../common/constants/constants')


describe('utils test', () => {

    // Negative test from create_file()
    it("Send other type of value in create_file", () => {
        let utils = new Utils();
        const file = {
            path: null,
            content: "print('hola mundo')"
        }
        expect(() => { utils.create_file(file); }).to.throw(CompilersServiceErrorFileSystem, "Function doesn't accept null parameter");
    });

    // Negative tests from get_data_file()
    it("Don't send parameters in get_data_file", () => {
        let utils = new Utils();
        expect(() => { utils.get_data_file(); }).to.throw(CompilersServiceError, "Function doesn't accept null parameters");
    });

    it("Send other type of parameters in get_data_file", () => {
        let utils = new Utils();
        expect(() => { utils.get_data_file(3,5); }).to.throw(CompilersServiceError, "Function needs a objects as parameters");
    });

    // Positive test from get_data_file()
    it("Don't send parameters in get_data_file", () => {
        let utils = new Utils();
        const project = {
            name: 'testProject',
            language: 'python'
        };
        const file = {
            file_name: 'testFile',
            content: "print('hola mundo')"
        };
        res_function = utils.get_data_file(project, file);
        res_expected = {
            path: constants.projects_path + '/testProject/testFile.py',
            content: file.content
        }
        expect(res_function).to.equal(res_function);
    });
    
    // Negative tests from get_file_path()
    it("Send null parameters in get_file_path", () => {
        let utils = new Utils();
        expect(() => { utils.get_data_file(null, null); }).to.throw(CompilersServiceError, "Function doesn't accept null parameters");
    });

    it("Don't send parameters in get_file_path", () => {
        let utils = new Utils();
        expect(() => { utils.get_data_file(); }).to.throw(CompilersServiceError, "Function doesn't accept null parameters");
    });

    it("Send other type of parameters in get_file_path", () => {
        let utils = new Utils();
        expect(() => { utils.get_data_file(3,5); }).to.throw(CompilersServiceError, "Function needs a objects as parameters");
    });

    // Positive test from get_file_path()
    it("Send valid parameters in get_file_path", () => {
        let utils = new Utils();
        const project = {
            name: 'testProject',
            language: 'java'
        };
        const file = {
            file_name: 'testFile'
        };
        res_function = utils.get_file_path(project, file);
        res_expected = constants.projects_path + '/testProject/testFile.java'
        expect(res_function).to.equal(res_function);
    });

    // Negative tests from validate_parameters()
    it("Send null parameters in validate_parameters", () => {
        let utils = new Utils();
        expect(() => { utils.validate_parameters(null, null); }).to.throw(CompilersServiceError, "Function doesn't accept null parameters");
    });

    it("Don't send parameters in validate_parameters", () => {
        let utils = new Utils();
        expect(() => { utils.validate_parameters(); }).to.throw(CompilersServiceError, "Function doesn't accept null parameters");
    });

    it("Send other type of parameters in validate_parameters", () => {
        let utils = new Utils();
        expect(() => { utils.validate_parameters(3,5); }).to.throw(CompilersServiceError, "Function needs a objects as parameters");
    });

    // Negative tests from validate_path()
    it("Send null parameter in validate_path", () => {
        let utils = new Utils();
        expect(() => { utils.validate_path(null); }).to.throw(CompilersServiceError, "Function doesn't accept null parameter");
    });

    it("Don't send parameter in validate_path", () => {
        let utils = new Utils();
        expect(() => { utils.validate_path(); }).to.throw(CompilersServiceError, "Function doesn't accept null parameter");
    });

    it("Send other type of parameter in validate_path", () => {
        let utils = new Utils();
        expect(() => { utils.validate_path(3); }).to.throw(CompilersServiceError, "File path should be String");
    });
});
