/*
@python_command_test.js Copyright (c) 2021 Jalasoft
2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
All rights reserved
This software is the confidential and proprietary information of
Jalasoft , Confidential Information "). You shall not
disclose such Confidential Information and shall use it only in
accordance with the terms of the license agreement you entered into
with Jalasoft
*/


const expect = require("chai").expect;
const PythonCommand = require('../../../../../core/compiler/command_factory/python_command');
const Parameters = require('../../../../../core/compiler/command_factory/parameters');
const CompilersServiceError = require('../../../../../common/errors/compilers_service_error');
const constants = require('../../../../../common/constants/constants');


describe('python command test', () => {

    it("build python command on null ",() => {
        let command = new PythonCommand();
        expect(() => { command.builder(null); })
        .to.throw(CompilersServiceError, 'command parameters in null or empty');
    });

    it("build python command on empty", () => {
        let command = new PythonCommand();
        expect(() => { command.builder(""); })
        .to.throw(CompilersServiceError, 'command parameters in null or empty');
    });

    it("build python command on an empty array", () => {
        let command = new PythonCommand();
        expect(() => { command.builder([]); })
        .to.throw(CompilersServiceError, 'command parameters in null or empty');
    });

    it("build python command with null parameters", () => {
        expect(() => { 
            let parameters = new Parameters(null , null, null);
            let command = new PythonCommand();
            command.builder(parameters); })
        .to.throw(CompilersServiceError, 'binary path in null or empty');
    });

    it("build python command", async() => {
        let parameters = new Parameters(constants.languages['python'].path, constants.projects_path, "projectName");
        let command = new PythonCommand();
        command = await command.builder(parameters);
        expect(command).to.be.an('array');
    });
});
