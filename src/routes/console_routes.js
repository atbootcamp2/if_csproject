/*
 @console_routes.js Copyright (c) 2021 Jalasoft
 2643 Av Melchor Perez de Olguin , Colquiri Sud, Cochabamba, Bolivia.
 Av. General Inofuentes esquina Calle 20,Edificio Union № 1376, La Paz, Bolivia
 All rights reserved

 This software is the confidential and proprietary information of
 Jalasoft , Confidential Information "). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Jalasoft
*/

'use strict'

const express = require('express');
const api = express.Router();
const ConsoleController = require('../controllers/console_controller');
const console_controller = new ConsoleController();

// Console methods
api.get('/console/project/:id', console_controller.get_console_result);

// Exports routes
module.exports = api;
